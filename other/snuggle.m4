## snuggle.m4 --- like guile.m4, but more portable
## serial 19
##
## Copyright (C) 2011-2015, 2017, 2020 Thien-Thi Nguyen
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

##: SNUGGLE_SET_SOFIXFLAGS([default-value])
##  SOFIXFLAGS
##
## Set shell var @code{SOFIXFLAGS} and mark it for
## substitution, as by @code{AC_SUBST}.
## Its value is based on the @code{host_os} shell variable:
##
## @table @code
## @item linux-gnu
## no-la,no-symlinks
## @end table
##
## If no matching @code{host_os} is found, use @code{default-value}.  For
## example, a conservative @code{default-value} would be @code{ln-s-lib},
## while a more optimistic one would be @code{no-la,no-symlinks}.  If
## @code{default-value} is not specified, it defaults to @code{ln-s-lib}.
##
## @xref{sofix}, and @xref{uninstall-sofixed}, for more info.
##
AC_DEFUN([SNUGGLE_SET_SOFIXFLAGS],[
AC_REQUIRE([AC_CANONICAL_HOST])
AC_CACHE_CHECK([sofix flags],[host_cv_sofixflags],
[AS_CASE([$host_os],
  [linux-gnu],[host_cv_sofixflags=no-la,no-symlinks],
  [host_cv_sofixflags=m4_default_nblank_quoted([$1],[ln-s-lib])])])
SOFIXFLAGS="$host_cv_sofixflags"
AC_SUBST([SOFIXFLAGS])
])

##: SNUGGLE_PROGS
##  GUILE GUILE_CONFIG GUILE_TOOLS
##
## Set paths to Guile interpreter, config and tool programs.
## Look for programs @shellcommand{guile}, @shellcommand{guile-config}
## and @shellcommand{guile-tools}, and set variables @code{GUILE},
## @code{GUILE_CONFIG} and @code{GUILE_TOOLS}, to their paths, respectively.
## However, if any of those vars are set on the command-line
## or in the environment, they take precedence.
## If either of the first two is not found, signal error.
##
## Mark the variables for substitution, as by @code{AC_SUBST}.
##
AC_DEFUN([SNUGGLE_PROGS],[
dnl
AC_ARG_VAR([GUILE],[the Guile interpreter program])dnl
AS_IF([test x = x"$GUILE"],[AC_PATH_PROG([GUILE],[guile])])
AS_IF([test x = x"$GUILE"],
      [AC_MSG_ERROR([guile required but not found])])
dnl
AC_ARG_VAR([GUILE_CONFIG],[the Guile configuration information script])dnl
AS_IF([test x = x"$GUILE_CONFIG"],[AC_PATH_PROG([GUILE_CONFIG],[guile-config])])
AS_IF([test x = x"$GUILE_CONFIG"],
      [AC_MSG_ERROR([guile-config required but not found])])
dnl
AC_ARG_VAR([GUILE_TOOLS],[the Guile tools dispatcher program])dnl
AS_IF([test x = x"$GUILE_TOOLS"],[AC_PATH_PROG([GUILE_TOOLS],[guile-tools])])
dnl
AC_SUBST([GUILE])
AC_SUBST([GUILE_CONFIG])
AC_SUBST([GUILE_TOOLS])
])

##: SNUGGLE_FLAGS
##  GUILE_CFLAGS GUILE_LDFLAGS
##
## Set flags for compiling and linking with Guile.
## This macro runs the program @shellcommand{guile-config}
## to find out where Guile's header files and libraries are installed.
## It sets two variables, @code{GUILE_CFLAGS} and @code{GUILE_LDFLAGS}.
##
## @table @code
## @item GUILE_CFLAGS
## Flags to pass to a C or C++ compiler to build code that
## uses Guile header files.  This is almost always just a @code{-I} flag.
##
## @item GUILE_LDFLAGS
## Flags to pass to the linker to link a program against Guile.
## This includes @code{-lguile} for the Guile library itself, any
## libraries that Guile itself requires (like -lqthreads), and so on.
## It may also include a @code{-L} flag to tell the compiler
## where to find the libraries.
## @end table
##
## The variables are marked for substitution, as by @code{AC_SUBST}.
##
AC_DEFUN([SNUGGLE_FLAGS],[
AC_REQUIRE([SNUGGLE_PROGS])dnl
AC_CACHE_CHECK([libguile compile flags],[snuggle_cv_compile],
[snuggle_cv_compile=`$GUILE_CONFIG compile`])
AC_CACHE_CHECK([libguile link flags],[snuggle_cv_link],
[snuggle_cv_link=`$GUILE_CONFIG link`])
GUILE_CFLAGS="$snuggle_cv_compile"
GUILE_LDFLAGS="$snuggle_cv_link"
AC_SUBST([GUILE_CFLAGS])
AC_SUBST([GUILE_LDFLAGS])
])

##: SNUGGLE_GUILE_LIBSITE_DIR(cache-var-prefix)
##  GUILE_LIBSITE
##
## Use Guile-BAUX program @shellcommand{re-prefixed-site-dirs}
## to set shell-variable @code{@var{cache-var-prefix}_cv_minstroot},
## which is subsequently also copied to var @code{GUILE_LIBSITE},
## marked for substitution, as by @code{AC_SUBST}.
##
AC_DEFUN([SNUGGLE_GUILE_LIBSITE_DIR],[
AC_REQUIRE([SNUGGLE_PROGS])
AC_CACHE_CHECK([module installation root dir],[$1_cv_minstroot],[
__snuggle_saved_GUILE_LOAD_PATH="$GUILE_LOAD_PATH"
GUILE_LOAD_PATH=
eval `GUILE="$GUILE" \
      $ac_aux_dir/guile-baux/gbaux-do \
      re-prefixed-site-dirs "$GUILE_CONFIG" $1`
GUILE_LOAD_PATH="$__snuggle_saved_GUILE_LOAD_PATH"
AS_UNSET([__snuggle_saved_GUILE_LOAD_PATH])
])
GUILE_LIBSITE="$][$1_cv_minstroot"
AC_SUBST([GUILE_LIBSITE])
])])

##: SNUGGLE_GUILE_SITE_CCACHE_DIR(cache-var-prefix)
##  GUILE_SITE_CCACHE
##
## Determine if Guile has @code{ccachedir} in its @code{%guile-build-info}
## alist.  If so, reprefix the value to start with @code{$libdir} and end
## with @code{site-ccache}, and copy to var @code{GUILE_SITE_CCACHE},
## marked for substitution, as by @code{AC_SUBST}.  If not available,
## the value is @code{no}.
##
AC_DEFUN([SNUGGLE_GUILE_SITE_CCACHE_DIR],[
AC_REQUIRE([SNUGGLE_PROGS])
AC_CACHE_CHECK([Guile site ccache dir],[$1_cv_site_ccache_dir],[
cat > conftest.scm <<EOF
(use-modules (srfi srfi-13))
(display
 (let ((libdir (assq-ref %guile-build-info 'libdir)))
   (cond ((assq-ref %guile-build-info 'ccachedir)
          => (lambda (ccachedir)
               (string-append
                "$" "{libdir}"
                (substring ccachedir
                           (string-length libdir)
                           (string-contains ccachedir "ccache"))
                "site-ccache")))
         (else ""))))
EOF
$1_cv_site_ccache_dir=$($GUILE -s conftest.scm 2>/dev/null)
AS_IF([test "$$1_cv_site_ccache_dir"],,[$1_cv_site_ccache_dir=no])
])
AC_SUBST([GUILE_SITE_CCACHE],[$$1_cv_site_ccache_dir])
])

##: SNUGGLE_GUILE_TOOLS_EXISTSP(cache-var,program)
##
## Check if @shellcommand{guile-tools} lists @var{program}.  If so, set
## shell variable @var{cache-var} to @code{yes}, otherwise @code{no}.
##
AC_DEFUN([SNUGGLE_GUILE_TOOLS_EXISTSP],[
AC_REQUIRE([SNUGGLE_PROGS])
AC_CACHE_CHECK([for "guile-tools $2"],[$1],
[AS_IF([$GUILE_TOOLS | grep "^$2$" 1>/dev/null 2>&1],[$1=yes],[$1=no])])
])])

##: SNUGGLE_CHECK(var,check)
##
## Evaluate Guile Scheme code and set a variable.
## Set @var{var} to @code{yes} or @code{no} depending on the return
## value of having @shellcommand{$GUILE -c} evaluate @var{check}.
##
## @var{check} is one or more Guile Scheme expressions, the last of which
## should return either @code{0} (zero) or non-@code{#f} to indicate success.
## Non-zero number or @code{#f} indicates failure.
## This is conventionally achieved by wrapping the last expression in
## @code{exit}.  For example, @code{(foo) (bar) (exit (baz))}.
##
## Avoid using the character @samp{#} (hash) since that can confuse
## Autoconf.  You can use @samp{@@%:@@} (at-percent-colon-at), instead.
## @xref{Quadrigraphs,,,autoconf}.
##
AC_DEFUN([SNUGGLE_CHECK],[
AC_REQUIRE([SNUGGLE_PROGS])
AS_IF([$GUILE -c "$2" > /dev/null 2>&1],
      [AS_VAR_SET([$1],[yes])],
      [AS_VAR_SET([$1],[no])])
])

##: SNUGGLE_MODULE_CHECK(var,module,featuretest,description)
##
## Check feature of a Guile Scheme module.
## Set @var{var} based on whether or not @var{module} supports @var{featuretest}.
## @var{var} is a shell variable name to be set to @code{yes} or @code{no}.
## Additionally, this value is cached in shell var @code{guile_cv_@var{var}}.
## @var{module} is a list of symbols sans parens, like: @code{ice-9 common-list}.
## @var{featuretest} is one or more Guile Scheme expressions,
## the last of which should evaluate to zero or non-@code{#f} for success,
## and non-zero or @code{#f} for failure.
## Unlike for @code{SNUGGLE_CHECK}, you do not need to use @code{exit}.
## @var{description} is a present-tense verb phrase to be passed to
## @code{AC_MSG_CHECKING}.
##
AC_DEFUN([SNUGGLE_MODULE_CHECK],[
AS_VAR_PUSHDEF([cv],[guile_cv_$1])dnl
AC_CACHE_CHECK([if ($2) $4],[cv],
[SNUGGLE_CHECK([cv],[(use-modules ($2)) (exit ((lambda () $3)))])])
AS_VAR_SET([$1],[$cv])
AS_VAR_POPDEF([cv])dnl
])

##: SNUGGLE_MODULE_AVAILABLE(module-name)
##
## Check availability of a Guile Scheme module.
## @var{module-name} is a list of symbols, without surrounding parens,
## like: @code{ice-9 common-list}.  This sets the shell variable
## @code{have_mod_@var{module-name}} to @code{yes} or @code{no}.
## Additionally, this value is cached in shell var
## @code{guile_cv_have_mod_@var{module-name}}.
## In the shell variable names, any ``strange characters'' (e.g., hyphen)
## in @var{module-name} are converted to underscore.
##
AC_DEFUN([SNUGGLE_MODULE_AVAILABLE],[
AS_VAR_PUSHDEF([var],[have_mod_$1])dnl
SNUGGLE_MODULE_CHECK(var,[$1],[0],[is available])
AS_VAR_POPDEF([var])dnl
])

##: SNUGGLE_CHECK_ICE9_OPTARGS(var)
##
## Check if module @code{(ice-9 optargs-kw)} is available.  If so, set
## shell var @var{var} to @code{no} (see why below).  Otherwise, check if
## module @code{(ice-9 optargs)} acts like @code{(ice-9 optargs-kw)}.
## If so, set @var{var} to @code{yes}, otherwise set it to @code{no}.
## Additionally, this value is cached in shell var @code{guile_cv_@var{var}}.
##
## Some versions of Guile provide a module @code{(ice-9 optargs)} that
## acts like @code{(ice-9 optargs-kw)} (and subsequently omit the latter,
## instead of providing both).  Code that uses @code{(ice-9 optargs-kw)}
## solely can be textually kludged to load @code{(ice-9 optargs)} in
## these situations if @var{var} has value @code{yes} (and you @code{AC_SUBST})
## it.  Here is a Makefile.am fragment that demonstrates the technique:
##
## @example
## install-data-hook:
##         if test "$(need_optargs_kludge)" = yes ; then \
##            sed s/optargs-kw/optargs/ foo.scm > TMP ; \
##            mv TMP foo.scm ; \
##         fi
## @end example
##
## In this example, @var{var} is @code{need_optargs_kludge}.  If it turns
## out @code{(ice-9 optargs-kw)} is available, @code{need_optargs_kludge}
## would have value @code{no}, and the kludge would neither be required nor
## applied.
##
AC_DEFUN([SNUGGLE_CHECK_ICE9_OPTARGS],[
SNUGGLE_MODULE_AVAILABLE([ice-9 optargs-kw])
AS_IF([test xyes = x"$have_mod_ice_9_optargs_kw"],
[$1=no],
[SNUGGLE_MODULE_CHECK([$1],[ice-9 optargs],
  [(= 2 ((lambda* (a @%:@:optional b) b) 4 2))],
  [acts like (ice-9 optargs-kw)])])
])

##: SNUGGLE_CHECK_META_SWITCH_MINUS_E_STRING(cache-var)
##
## Check if meta-switch invocation can handle @code{-e STRING}.
## If so, set @var{cache-var} to @code{yes}, otherwise @code{no}.
##
AC_DEFUN([SNUGGLE_CHECK_META_SWITCH_MINUS_E_STRING],[
AC_REQUIRE([SNUGGLE_PROGS])
AC_CACHE_CHECK([if meta-switch parsing handles -e STRING],[$1],[
cat > conftest-dwms <<EOF
@%:@! $GUILE \\
-e "(a b c)" -s
!@%:@
 (define-module (a b c) @%:@:export (main))
 (define (main args) (exit @%:@t))
EOF
chmod +x conftest-dwms
AS_IF([./conftest-dwms 1>conftest-dwms.out 2>conftest-dwms.err],
      [$1=yes],
      [$1=no])
])])

##: SNUGGLE_GUILE_USER_PROVIDES(var,name)
##
## Check if module @code{(guile-user)} provides @var{name}.
## If so, set @var{var} to "yes", otherwise "no".
## Additionally, this value is cached in shell var @code{guile_cv_@var{var}}.
##
AC_DEFUN([SNUGGLE_GUILE_USER_PROVIDES],
[SNUGGLE_MODULE_CHECK([$1],[guile-user],[$2],[provides ‘$2’])])

##: SNUGGLE_CHECK_CLASSIC_HEADERS
##  HAVE_GUILE_GH_H HAVE_GUILE_MODSUP_H
##
## Check for @file{guile/gh.h} and @file{guile/modsup.h} via
## @code{AC_CHECK_HEADERS}, thus @code{#define}ing the
## C preprocessor symbols @code{HAVE_GUILE_GH_H} and
## @code{HAVE_GUILE_MODSUP_H}, respectively.
##
## The checks respect var @code{GUILE_CFLAGS}
## (from macro @code{SNUGGLE_FLAGS}).
##
AC_DEFUN([SNUGGLE_CHECK_CLASSIC_HEADERS],[
__snuggle_saved_CFLAGS="$CFLAGS"
CFLAGS="$GUILE_CFLAGS $CFLAGS"
AC_CHECK_HEADERS([guile/gh.h],,,[[
#include <libguile.h>
]])
AC_CHECK_HEADERS([guile/modsup.h],,,[[
#include <libguile.h>
#if HAVE_GUILE_GH_H
#include <guile/gh.h>
#endif
]])
CFLAGS="$__snuggle_saved_CFLAGS"
AS_UNSET([__snuggle_saved_CFLAGS])
])

##: SNUGGLE_MAINT_MODE_BASE(description)
##
## Add support for @file{configure} script option
## @code{--enable-maint-mode} via @code{AC_ARG_ENABLE}.
## This provides shell var @code{enable_maint_mode}
## with default value @code{no},
## and arranges to display @var{description} in the
## @shellcommand{--help} output.
##
AC_DEFUN([SNUGGLE_MAINT_MODE_BASE],[
AC_ARG_ENABLE([maint-mode],
[AS_HELP_STRING([--enable-maint-mode],[$1])],,[enable_maint_mode=no])
])

##: SNUGGLE_MAINT_MODE
##  MAINT_MODE
##
## Add support for @file{configure} script option
## @code{--enable-maint-mode}, and provide the Automake
## conditional var @code{MAINT_MODE}.  Note that this is similar
## to the latter part of @code{AM_MAINTAINER_MODE}; the former part,
## which controls @file{Makefile}-rebuilding rules, is not included.
##
AC_DEFUN([SNUGGLE_MAINT_MODE],[
SNUGGLE_MAINT_MODE_BASE([enable makefile rules to build
 maintainer-specific bits unlikely to be interesting to casual users])
AM_CONDITIONAL([MAINT_MODE],[test x"$enable_maint_mode" = xyes])
])

## snuggle.m4 ends here
