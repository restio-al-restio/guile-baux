/* level.h --- define ‘GI_LEVEL’ et al
   serial 4

   Copyright (C) 2012, 2014, 2017, 2020 Thien-Thi Nguyen

   This is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SCM_MAJOR_VERSION
#define SCM_MAJOR_VERSION  1
#endif

#ifndef SCM_MINOR_VERSION
#define SCM_MINOR_VERSION  4
#endif

#define GI_LEVEL(major,minor)                   \
  (((major << 8)                                \
    + minor)                                    \
   <= ((SCM_MAJOR_VERSION << 8)                 \
       + SCM_MINOR_VERSION))

#define GI_LEVEL_1_8  GI_LEVEL (1, 8)

#define GI_LEVEL_PRECISELY_1_8  (GI_LEVEL_1_8 && ! GI_LEVEL (1, 9))

#define GI_LEVEL_2_0  GI_LEVEL (2, 0)

#define GI_LEVEL_2_2  GI_LEVEL (2, 2)

#define GI_LEVEL_3_0  GI_LEVEL (3, 0)

/* level.h ends here */
