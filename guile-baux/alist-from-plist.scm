;;; alist-from-plist.scm

;; Copyright (C) 2011, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (guile-baux alist-from-plist)
  #:export (alist<-plist))

;; Return @var{plist} as an association list with order reversed.
;; For example:
;;
;; @example
;; (alist<-plist '(p1 v1 p2 v2))
;; @result{} ((p2 . v2) (p1 . v1))
;; @end example
;;
(define (alist<-plist plist)
  (let loop ((acc '()) (ls plist))
    (if (null? ls)
        acc
        (loop (acons (car ls) (cadr ls) acc)
              (cddr ls)))))

;;; alist-from-plist.scm ends here
