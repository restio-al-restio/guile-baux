;;; pke.scm

;; Copyright (C) 2010, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (guile-baux pke)
  #:export (pke)
  #:use-module ((guile-baux common) #:select (fse)))

;; Like @code{pk}, except output goes to current error port.
;;
(define (pke . x)
  (fse "~%;;; ~S~%" x)
  (car (last-pair x)))

;;; pke.scm ends here
