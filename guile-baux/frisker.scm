;;; frisker.scm

;; Copyright (C) 2010, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (guile-baux frisker)
  #:export (frisker
            mod-up-ls mod-down-ls mod-int?
            edge-type edge-up edge-down)
  #:use-module ((srfi srfi-1) #:select (filter remove))
  #:use-module ((guile-baux scheme-scanner) #:select (scheme-scanner))
  #:use-module ((guile-baux common) #:select (fs fse)))

(define DEFAULT-MODULE '(guile-user))

(define (grok-proc default-module note-use!)
  (let ((next-form (scheme-scanner)))

    (lambda (filename)
      (let ((p (open-input-file filename))
            (curmod #f))

        (define (ok! type u)
          (note-use! type (or curmod default-module) u))

        (define (regular! use)
          (ok! 'regular
               ;; Handle both ((foo bar) #:select ...)
               ;;         and  (foo bar).
               (let ((maybe (car use)))
                 (if (pair? maybe)
                     maybe
                     use))))

        (define (problem blurb)
          (fse "~A: ~A~%" filename blurb))

        (let loop ()
          (let ((form (catch #t (lambda ()
                                  (next-form p))
                             (lambda x
                               (problem (car x))
                               #f))))
            (cond ((not (pair? form))
                   (close-port p))
                  ((not (eq? 'form (car form)))
                   (loop))
                  ((begin (set! form (assq-ref (cdr form) 'sexp))
                          (not (pair? form)))
                   (if (and (symbol? form)
                            (string-index (symbol->string form) #\nul))
                       (problem "detected NUL in symbol")
                       (loop)))
                  (else
                   (case (car form)
                     ((define-module)
                      (set! curmod (cadr form))
                      (ok! 'def #f)
                      (let dm-loop ((ls (cddr form)))
                        (or (null? ls)
                            (case (car ls)
                              ((:use-module #:use-module)
                               (regular! (cadr ls))
                               (dm-loop (cddr ls)))
                              ((:autoload #:autoload)
                               (ok! 'autoload (cadr ls))
                               (dm-loop (cdddr ls)))
                              (else (dm-loop (cdr ls)))))))
                     ((use-modules)
                      (for-each regular! (cdr form)))
                     ((load primitive-load)
                      (ok! 'computed
                           (let ((file (cadr form)))
                             (if (string? file)
                                 file
                                 (fs "[computed in ~A]" filename))))))
                   (loop)))))))))

(define up-ls (make-object-property))   ; list
(define dn-ls (make-object-property))   ; list
(define int?  (make-object-property))   ; defined via ‘define-module’

(define (ixsel sel)
  (lambda (ls)
    (sel int? ls)))

(define i-only (ixsel filter))
(define x-only (ixsel remove))

;; These are separate funcs rather than simple aliases
;; (or direct exports) to render them "read-only".
;; TODO: Look into lifting this restriction.

;; Return the upstream modules list of @var{module}.
(define (mod-up-ls   module) (up-ls module))
;; Return the downstream modules list of @var{module}.
(define (mod-down-ls module) (dn-ls module))
;; Return @code{#t} if @var{module} is @dfn{internal}
;; (has a @code{define-module} form).
(define (mod-int?    module) (int?  module))

(define etype (make-object-property))   ; symbol

;; Return the symbolic type of @var{edge}, one of
;; @code{regular}, @code{autoload}, @code{computed}.
(define (edge-type edge) (etype edge))

(define (make-edge type up down)
  (let ((new (cons up down)))
    (set! (etype new) type)
    new))

;; Return the upstream-side module of @var{edge}.
(define (edge-up   edge) (car edge))
;; Return the downstream-side module of @var{edge}.
(define (edge-down edge) (cdr edge))

(define-macro (pile! object place)
  `(set! ,place (cons ,object ,place)))

(define (scan default-module files)
  (let ((modules (list))
        (edges (list)))
    (define (canonical module)
      (and=> (member module modules) car))
    (define (intern module)
      (cond ((canonical module))
            (else (set! (up-ls module) (list))
                  (set! (dn-ls module) (list))
                  (pile! module modules)
                  module)))
    (define (note-use! type d u)
      (let ((d (intern d)))
        (if (eq? type 'def)
            (set! (int? d) #t)
            (let* ((u (intern u))
                   (edge (make-edge type u d)))
              (pile! edge edges)
              (pile! edge (up-ls d))
              (pile! edge (dn-ls u))))))
    (for-each (grok-proc default-module note-use!)
              files)
    (let* ((all-dn (delay (map edge-down edges)))
           (all-up (delay (map edge-up   edges)))
           (details `((internal . ,(delay (i-only modules)))
                      (external . ,(delay (x-only modules)))
                      (i-up     . ,(delay (i-only (force all-dn))))
                      (x-up     . ,(delay (x-only (force all-dn))))
                      (i-down   . ,(delay (i-only (force all-up))))
                      (x-down   . ,(delay (x-only (force all-up)))))))
      ;; rv
      (lambda (key)
        (if (pair? key)
            (canonical key)
            (case key
              ((modules) modules)
              ((edges)   edges)
              (else (and=> (assq-ref details key) force))))))))

;; Return a procedure @var{frisk} that takes a list of files.
;; @var{options} is an alist.  Recognized keys are:
;;
;; @table @code
;; @vindex default-module
;; @item default-module
;; The module to use as default module instead of @code{(guile-user)}.
;; @end table
;;
;; @var{frisk} returns another procedure @var{report}, that takes
;; one arg, @var{request} (a symbol), and returns:
;;
;; @example
;; modules    @r{entire list of modules}
;; internal   @r{list of internal modules}
;; external   @r{list of external modules}
;; i-up       @r{list of modules upstream of internal modules}
;; x-up       @r{list of modules upstream of external modules}
;; i-down     @r{list of modules downstream of internal modules}
;; x-down     @r{list of modules downstream of external modules}
;; edges      @r{list of edges}
;; @end example
;;
;; Note that @code{x-up} will always return the empty list, since by
;; (lack of) definition, we only know external modules by reference.
;;
;; The module and edge objects managed by @var{report} can be examined in
;; detail by using the other procedures described below.  Be careful not to
;; confuse a freshly consed list of symbols, like @code{(a b c)} with the
;; @dfn{managed module} @code{(a b c)}.  For this reason, @var{request}
;; may also be a list of symbols (i.e., a module name), in which case
;; @var{report} returns the managed module.
;;
(define (frisker . options)
  (let ((default-module (or (assq-ref options 'default-module)
                            DEFAULT-MODULE)))
    (lambda (files)
      (scan default-module files))))

;;; frisker.scm ends here
