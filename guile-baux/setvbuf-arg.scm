;;; setvbuf-arg.scm

;; Copyright (C) 2021 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Guile 2.2 changed the arg accepted by ‘setvbuf’ from the set:
;;
;;  - _IONBF
;;  - _IOLBF
;;  - _IOFBF
;;
;; to the (corresponding) set:
;;
;;  - 'none
;;  - 'line
;;  - 'block
;;
;; That is, the previous "constant variables" were replaced
;; by symbols.  This module exports the abstractions:
;;
;;  - *non-buffered*
;;  - *line-buffered*
;;  - *block-buffered*
;;
;; which expand (via ‘cond-expand’) to one of the above sets
;; depending on ‘cond-expand’ feature ‘guile-2.2’.

;;; Code:

(define-module (guile-baux setvbuf-arg)
  #:export (*non-buffered*
            *line-buffered*
            *block-buffered*))

;; NB: The ts "category" for the following should be "constant object".

(define *non-buffered*
  (cond-expand (guile-2.2 'none)
               (else _IONBF)))

(define *line-buffered*
  (cond-expand (guile-2.2 'line)
               (else _IOLBF)))

(define *block-buffered*
  (cond-expand (guile-2.2 'block)
               (else _IOFBF)))

;;; setvbuf-arg.scm ends here
