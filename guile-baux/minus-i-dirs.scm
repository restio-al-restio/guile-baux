;;; minus-i-dirs.scm

;; Copyright (C) 2014, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (guile-baux minus-i-dirs)
  #:export (minus-i-dirs)
  #:use-module ((ice-9 regex) #:select (match:substring
                                        fold-matches)))

(define MINUS-I (make-regexp "-I *([^ ]+)"))

;; Return a list of @code{-I} directories extracted from @var{string}.
;; Directory names must not contain whitespace.
;; For example:
;;
;; @example
;; (minus-i-dirs "-DDEBUG -I/tmp/headers -I /usr/include -I .")
;; @result{} ("/tmp/headers" "/usr/include" ".")
;; @end example
;;
;; Note that the result maintains input order.
;;
(define (minus-i-dirs string)
  (reverse! (fold-matches MINUS-I string '()
                          (lambda (m acc)
                            (cons (match:substring m 1)
                                  acc)))))

;;; minus-i-dirs.scm ends here
