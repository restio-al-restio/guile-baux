;;; elide-dot-dotdot.scm

;; Copyright (C) 2010, 2017 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (guile-baux elide-dot-dotdot)
  #:export (filename-components
            filename-components-append
            elide-dot-dotdot)
  #:use-module ((srfi srfi-13) #:select (string-tokenize string-join))
  #:use-module ((srfi srfi-14) #:select (char-set-complement char-set)))

;; Return a list of filename components parsed from @var{string}.
;; Components are delimited by "/", which is discarded.
;; Null string components are also discarded.
;;
(define filename-components
  (let ((split (char-set-complement (char-set #\/))))
    (lambda (string)
      (string-tokenize string split))))

;; Return a string composed by prefixing each element of @var{ls} with "/".
;;
(define (filename-components-append ls)
  (string-join ls "/" 'prefix))

;; Return a new string made by removing file name components from string
;; @var{abs-name} that are @samp{.} (dot), as well as file name components
;; followed by @samp{..} (dotdot), along with the @samp{..} itself; note that
;; these simplifications are done without checking the resulting file names in
;; the file system.
;;
;; The returned string normally does not end in @samp{/} (slash),
;; aside from the case where it names the root directory "/".
;; However, if @var{need-trailing-sep?} is non-@code{#f}, it does.
;;
(define (elide-dot-dotdot abs-name need-trailing-sep?)
  (let loop ((comps (reverse! (filename-components abs-name)))
             (omit 0)
             (acc (if need-trailing-sep? ; blech
                      (list "")
                      (list))))
    (if (null? comps)
        (filename-components-append acc)
        (cond ((string=? "." (car comps))
               (loop (cdr comps) omit acc))
              ((string=? ".." (car comps))
               (loop (cdr comps) (1+ omit) acc))
              ((< 0 omit)
               (loop (cdr comps) (1- omit) acc))
              (else
               (loop (cdr comps) omit (cons (car comps) acc)))))))

;;; elide-dot-dotdot.scm ends here
